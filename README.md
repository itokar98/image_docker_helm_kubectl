# image_docker_helm_kubectl
Данный образ используется для управления кластером.

В него входит:
1. Kubectl
2. Helm

Для сборки образа используется gitlab ci/cd.
После сборки образ отправляется gitlab-registry.
